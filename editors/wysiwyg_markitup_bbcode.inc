<?php

/**
 * @file
 *   WYSIWYG MarkItUp BBCode editor-related hooks.
 */

/**
 * Implements hook_editor().
 */
function wysiwyg_markitup_bbcode_wysiwyg_markitup_bbcode_editor() {
  $editor['markitup_bbcode'] = array(
    'title' => 'markItUp - BBCode',
    'vendor url' => 'http://markitup.jaysalvat.com',
    'download url' => 'http://markitup.jaysalvat.com/downloads',
    'library path' => wysiwyg_get_path('markitup'),
    'libraries' => array(
      '' => array(
        'title' => 'Source',
        'files' => array('markitup/jquery.markitup.js'),
      ),
      'pack' => array(
        'title' => 'Packed',
        'files' => array('markitup/jquery.markitup.pack.js'),
      ),
    ),
    'version callback' => 'wysiwyg_markitup_bbcode_version',
    'themes callback' => 'wysiwyg_markitup_themes',
    'settings callback' => 'wysiwyg_markitup_bbcode_settings',
    'plugin callback' => 'wysiwyg_markitup_bbcode_plugins',
    'versions' => array(
      '1.1.5' => array(
        'js files' => array('markitup.js'),
      ),
    ),
  );
  return $editor;
}

function wysiwyg_markitup_bbcode_version($editor) {
  $version = wysiwyg_markitup_version($editor);
  return $version;
}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $config
 *   An array containing wysiwyg editor profile settings.
 * @param $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return
 *   A settings array to be populated in
 *   Drupal.settings.wysiwyg.configs.{editor}
 */
function wysiwyg_markitup_bbcode_settings($editor, $config, $theme) {
  drupal_add_css(drupal_get_path('module', 'wysiwyg_markitup_bbcode') . '/editors/css/style.css', array(
    // Specify an alternate basename; otherwise, style.css would override a
    // commonly used style.css file of the theme.
    'basename' => 'markitup.wysiwyg_markitup_bbcode.style.css',
    'group' => CSS_THEME,
  ));

  $settings = array(
    'root' => base_path() . $editor['library path'] . '/markitup/',
    'nameSpace' => $theme,
    'markupSet' => array(),
  );

  // Add configured buttons or all available.
  $default_buttons = array(
    'bold' => array(
      'name' => t('Bold'),
      'className' => 'markitup-bold',
      'key' => 'B',
      'openWith' => '[b]',
      'closeWith' => '[/b]',
    ),
    'italic' => array(
      'name' => t('Italic'),
      'className' => 'markitup-italic',
      'key' => 'I',
      'openWith' => '[i]',
      'closeWith' => '[/i]',
    ),
    'image' => array(
      'name' => t('Image'),
      'className' => 'markitup-image',
      'key' => 'P',
      'replaceWith' => '[img][![Source:!:http://]!][/img]',
    ),
    'link' => array(
      'name' => t('Link'),
      'className' => 'markitup-link',
      'key' => 'K',
      'openWith' => '[url=[![Link:!:http://]!]]',
      'closeWith' => '[/url]',
      'placeHolder' => 'Your text to link...',
    ),
    'list-bullet' => array(
      'name' => t('Bullet list'),
      'className' => 'markitup-list-bullet',
      'openWith' => '[list]',
      'closeWith' => '[/list]',
    ),
    'list-numeric' => array(
      'name' => t('Numeric list'),
      'className' => 'markitup-list-numeric',
      'openWith' => '[ol]',
      'closeWith' => '[/ol]',
    ),
    'quote' => array(
      'name' => t('Quote'),
      'className' => 'markitup-quote',
      'call' => '$.wmbQuote',
      'icon file' => 'quote.png',
      'icon path' => drupal_get_path('module', 'wysiwyg_markitup_bbcode') .'/images',
      'js file' => 'quote.js',
      'js path' => drupal_get_path('module', 'wysiwyg_markitup_bbcode') .'/editors/js',
    ),
    'youtube' => array(
      'name' => t('YouTube'),
      'className' => 'markitup-youtube',
      'openWith' => '[youtube]',
      'closeWith' => '[/youtube]',
      'icon file' => 'youtube.gif',
      'icon path' => drupal_get_path('module', 'wysiwyg_markitup_bbcode') .'/images',
    ),
  );
  $settings['markupSet'] = array();
  if (!empty($config['buttons'])) {
    foreach ($config['buttons'] as $plugin) {
      foreach ($plugin as $button => $enabled) {
        if (isset($default_buttons[$button])) {
          $settings['markupSet'][$button] = $default_buttons[$button];
        }
      }
    }
  }

  // Add related JS
  foreach ($settings['markupSet'] as $button => $button_settings) {
    if (isset($button_settings['js path']) && isset($button_settings['js file'])) {
      drupal_add_js($button_settings['js path'] .'/'. $button_settings['js file']);
    }
  }

  return $settings;
}

/**
 * Return internal plugins for this editor; semi-similar to hook_wysiwyg_plugin().
 */
function wysiwyg_markitup_bbcode_plugins($editor) {
  return array(
    'default' => array(
      'buttons' => array(
        'bold' => t('Bold'),
        'italic' => t('Italic'),
        'image' => t('Image'),
        'link' => t('Link'),
        'list-bullet' => t('Bullet list'),
        'list-numeric' => t('Numeric list'),
        'quote' => t('Quote'),
        'youtube' => t('YouTube'),
      ),
      'internal' => TRUE,
    ),
  );
}

