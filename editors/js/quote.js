(function($) {

  function wmb_getSelectionText() {
    text = "";
    if (window.getSelection) {
      text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
      text = document.selection.createRange().text;
    }
    return text;
  }

  $.wmbQuote = function() {
    textarea_selector = '.comment-form .field-name-comment-body textarea.markItUpEditor';
    // Is this a node form? If so, change the selector.
    if ($('.node-form')) {
      textarea_selector = '.node-form .field-name-body textarea.markItUpEditor';
    }

    // Retrieve and set the quote contents.
    $('li.markitup-quote a').click( function() {
      text = wmb_getSelectionText();
      text = "[quote]" + text + "[/quote]";
      current_val = $(textarea_selector).val().trim();
      new_val = current_val;
      // Add newlines if the text area already has a value.
      if (new_val) {
        new_val = new_val + "\n\n" + text;
      }
      else {
        new_val = text;
      }
      // Set the textarea with the new value.
      $(textarea_selector).val(new_val);
      return false;
    });
  }

})(jQuery);