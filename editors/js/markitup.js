(function($) {

/**
 * Attach this editor to a target element.
 */
Drupal.wysiwyg.editor.attach.markitup_bbcode = function(context, params, settings) {
  Drupal.wysiwyg.editor.attach.markitup(context, params, settings);
};

/**
 * Detach a single or all editors.
 */
Drupal.wysiwyg.editor.detach.markitup_bbcode = function (context, params, trigger) {
  Drupal.wysiwyg.editor.detach.markitup(context, params, trigger);
};

Drupal.wysiwyg.editor.instance.markitup_bbcode = {
  insert: function (content) {
    $.markItUp({ replaceWith: content });
  },

  setContent: function (content) {
    $('#' + this.field).val(content);
  },

  getContent: function () {
    return $('#' + this.field).val();
  }
};

})(jQuery);
